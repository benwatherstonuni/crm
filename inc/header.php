<!DOCTYPE html>
<html>
<head>
  <title><?php echo $page; ?> - CRM</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" media="all" href="../crm/assets/application.css" data-turbolinks-track="true" />
  <link rel="stylesheet"  href="../crm/assets/style.css" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-param" content="authenticity_token" />
<meta name="csrf-token" content="IFRZVBeLaNaYLcbFz8t5sjgaH7Hv+SO3L/v5UG/VPVzDjdxH4GhrOafPK1WnKE1qxWFm2cEZoqu8Ysfr3Kemrg==" />
</head>

<body cz-shortcut-listen="true">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="contacts">CRM</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
				<li class="<?php echo ($page == "Contacts" ? "active" : ""); ?>"><a href="contacts.php">Contacts</a></li>
				<li class="<?php echo ($page == "Projects" ? "active" : ""); ?>"><a href="projects.php">Projects</a></li>
				<li class="<?php echo ($page == "Businesses" ? "active" : ""); ?>"><a href="businesses.php">Businesses</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="contacts.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="contacts">Edit account</a></li>
            <li role="separator" class="divider"></li>
            <li><a rel="nofollow" data-method="delete" href="index.php">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>