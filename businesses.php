<?php 
$page = "Businesses";
include('inc/header.php');
 ?>

<div class="container">
	<p id="notice"></p>

<h1>Listing Businesses</h1>

<table id="businesses" class="table table-striped">
  <thead>
    <tr>
      <th>Name</th>
      <th>Address</th>
      <th>Timezone</th>
      <th>Active project?</th>
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
      <tr>
        <td data-label="Name">Smaller Earth Group</td>
        <td data-label="Address">Suite 4 Church House, 1 Hanover Street, Liverpool, L1 3DN</td>
        <td data-label="Timezone">London</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/1/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/1">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Work and Traveller</td>
        <td data-label="Address">Maximilianstraße 45a 13187 Berlin</td>
        <td data-label="Timezone">Berlin</td>
        <td data-label="Active"><span class="text-success glyphicon glyphicon-ok" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/2/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/2">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Roy Castle Lung Cancer Foundation</td>
        <td data-label="Address">4-6 Enterprise Way, Wavertree Technology Park, Liverpool, Merseyside, L13 1FB</td>
        <td data-label="Timezone">London</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/3/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/3">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Smaller Earth DE</td>
        <td data-label="Address">Smaller Earth GmbH, Maximilianstraße 45a, 13187 Berlin</td>
        <td data-label="Timezone">Berlin</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/4/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/4">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">USA Summer Camp</td>
        <td data-label="Address">157 Warwick Rd, Solihull, West Midlands, B92 7AR</td>
        <td data-label="Timezone">London</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/5/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/5">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Human Utopia</td>
        <td data-label="Address">1 Downham Rd S, Wirral, CH60 5RG</td>
        <td data-label="Timezone">London</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/6/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/6">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Total Swimming</td>
        <td data-label="Address">3 Frecheville Court, Off Knowsley Street, Bury, BL9 0UF</td>
        <td data-label="Timezone">London</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-business.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/businesses/7/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/businesses/7">Destroy</a></td>
      </tr>
  </tbody>
</table>

<br>

<a class="btn btn-primary pull-right" href="http://crm.cleversteam.com/businesses/new">New Business</a>

</div>


<?php include('inc/footer.php') ?>