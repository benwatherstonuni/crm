<?php 
$page = "Businesses";
include('inc/header.php');
 ?>

<div class="container">
	<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">Business details</div>
			<div class="panel-body">
				<p>Name: Smaller Earth Group</p>
				<p>Address: Suite 4 Church House, 1 Hanover Street, Liverpool, L1 3DN</p>
				<p>Timezone: London</p>
				<p>
					Total spend <span class="glyphicon glyphicon-question-sign" title="proposal_approved, active, pending_review, closed"></span>: £0.00
</p>				<a href="http://crm.cleversteam.com/businesses/1/edit">Edit</a>
			</div><!-- panel-body -->
		</div><!-- panel -->
		<div class="panel panel-default">
			<div class="panel-heading">Contacts</div>
			<ul class="list-group">
					<li class="list-group-item"><a href="http://crm.cleversteam.com/contacts/1">Mike Peters</a></li>
					<li class="list-group-item"><a href="http://crm.cleversteam.com/contacts/2">Bastian Weinberger</a></li>
			</ul>
		</div><!-- panel -->
		
	</div><!-- col -->
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
	<div class="panel-heading">Projects</div>
	<ul class="list-group">
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/16">Camp Side - Hanover</a></li>
	</ul>
</div><!-- panel -->


	</div><!-- col -->
</div><!-- row -->

<div class="row">
	<a href="businesses.php" class="btn-bottom">&lt;&lt; Back</a>
</div>

</div>

<?php include('inc/footer.php') ?>