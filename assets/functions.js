(function() {
  $('.table').html(function(index, html) {
    return html.replace(/(Show)/g, '<span class="glyphicon glyphicon-eye-open">').replace(/(Edit)/g, '<span class="glyphicon glyphicon-edit">').replace(/(Destroy)/g, '<span class="glyphicon glyphicon-trash">');
  });

}).call(this);
