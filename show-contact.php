<?php 
$page = "Contacts";
include('inc/header.php');
 ?>

<div class="container">
	<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">Contact details</div>
			<div class="panel-body">
				<span class="glyphicon glyphicon-user pull-left" aria-hidden="true" title="Image of user here maybe?" style="font-size: 80px; margin-right: 10px;"></span>
				<div class="pull-left">
					<p>Name: Christian Wack</p>
					<p>Number: </p>
					<p>Email: c.wack@smallerearth.com</p>
					<p>
						Total spend <span class="glyphicon glyphicon-question-sign" title="proposal_approved, active, pending_review, closed"></span>: £0.00
</p>				</div>
				
				<div style="clear:both"></div>
				<a href="http://crm.cleversteam.com/contacts/3/edit">Edit</a>
			</div><!-- panel-body -->
		</div><!-- panel -->
		<div class="panel panel-default">
			<div class="panel-heading">Attached businesses</div>
			<ul class="list-group">
					<li class="list-group-item"><a href="http://crm.cleversteam.com/businesses/2">Work and Traveller</a></li>
					<li class="list-group-item"><a href="http://crm.cleversteam.com/businesses/4">Smaller Earth DE</a></li>
			</ul>
		</div><!-- panel -->

		
	</div><!-- col -->
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
	<div class="panel-heading">Projects</div>
	<ul class="list-group">
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/4">Front End Website 2.0</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/5">Flights Module</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/6">Front End Website 1.0</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/7">Au Pair Front End</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/8">Front End Updates 1.5</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/9">Branding</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/14">Luggage Tag</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/15">Ticket Holder</a></li>
			<li class="list-group-item"><a href="http://crm.cleversteam.com/projects/17">External Linking and Menu Updates</a></li>
	</ul>
</div><!-- panel -->


	</div>
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">Comms</div>
			<ul class="list-group">
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
					<li class="list-group-item">Comms log here maybe?</li>
			</ul>
		</div><!-- panel -->
	</div>
</div><!-- row -->

<div class="row">
	<a href="contacts.php" class="btn-bottom">&lt;&lt; Back</a>
</div>

</div>

<?php include('inc/footer.php') ?>