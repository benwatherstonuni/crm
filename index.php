<!DOCTYPE html>
<!-- saved from url=(0037)http://crm.cleversteam.com/businesses -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>CRM - <?php echo $page;?></title>
  <link rel="stylesheet" media="all" href="assets/application.css" data-turbolinks-track="true">
    <link rel="stylesheet" href="assets/style.css">
  <script src="assets/application.js" data-turbolinks-track="true"></script><style type="text/css"></style>
  <meta name="csrf-param" content="authenticity_token">
<meta name="csrf-token" content="xMz7vKbyRU4+Oab43D+m26JdCpeQs7nkEW7FF9FyGQHJqAI1SQbYnAUuL1Itlkn+q6lTFiCkYfCgXb4QkeAedQ==">
</head>
<body cz-shortcut-listen="true">

<div class="container">
    <div class="login-container">
  <h2>Log in</h2>

<form class="new_user" id="new_user" action="contacts.php" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="K/WO7sPDmxgS5rV5rqO2pecoZNDKID/OZZD2s8EvVZAcEUYMyYgREDQeEy37Y5XdNYic4rMIrvGFDglPZjXKSw==" />
  <div class="field">
    <input autofocus="autofocus" type="email" value="" name="user[email]" id="user_email" placeholder="Email" />
  </div>

  <div class="field">
    <input autocomplete="off" type="password" name="user[password]" id="user_password" placeholder="Password" />
  </div>

    <div class="field">
      <input name="user[remember_me]" type="hidden" value="0" /><input type="checkbox" value="1" name="user[remember_me]" id="user_remember_me" />
      <label for="user_remember_me">Remember me</label>
    </div>

  <div class="actions">
    <input type="submit" name="commit" value="Log in" class="btn-primary fa-sign-in" />
  </div>
</form>

<div class="form-btn-container">
  <a href="/users/sign_up">Sign up</a><span> | </span>
  <a href="/users/password/new">Forgot your password?</a>
    </div>    
</div>
    
    
</div>

</body>

</html>