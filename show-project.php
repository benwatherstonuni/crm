<?php 
$page = "Projects";
include('inc/header.php');
 ?>


<div class="container">
	<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">External Linking and Menu Updates</div>
			<div class="panel-body">
				<p>Person responsible: dean@cleversteam.com</p>
				<p>
					Client stakeholder: <a href="http://crm.cleversteam.com/contacts/3">Christian Wack (Work and Traveller)</a>
</p>				<p>Created: 05 Nov 12:53</p>
				<p>Status: Pending review</p>
				<p>Amount: £60.00</p>
				<a href="http://crm.cleversteam.com/projects/17/edit">Edit</a>
			</div><!-- panel-body -->
		</div><!-- panel -->
		<div class="panel panel-default">
			<div class="panel-heading">Key dates</div>
			<div class="panel-body">
				<p>Starts on: 0005-11-15</p>
				<p>Due on: </p>
			</div><!-- panel-body -->
		</div><!-- panel -->

		
	</div><!-- col -->
	<div class="col-xs-12 col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">Activity log</div>
			<ul class="list-group">
				<li class="list-group-item">
	<p>status changed from active to pending_review by   at 2015-11-08 20:03:36 +0000</p>
	<small>
		  at 08 Nov 20:03
</small></li>
<li class="list-group-item">
	<p>status changed from proposal_sent to active by bob at 2015-11-05 15:13:08 +0000</p>
	<small>
		  at 05 Nov 15:13
</small></li>

			</ul>
		</div><!-- panel -->
	</div><!-- col -->
</div><!-- row -->

<div class="row">
	<a href="projects.php" class="btn-bottom">&lt;&lt; Back</a>
</div>

</div>

<?php include('inc/footer.php') ?>