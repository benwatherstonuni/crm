<?php 
$page = "Projects";
include('inc/header.php');
 ?>

<div class="container">
	<p id="notice"></p>

<h1>Listing Projects</h1>

<table class="table table-stiped">
  <thead>
    <tr>
      <th>Name</th>
      <th>Through</th>
      <th>Status</th>
      <th>Amount</th>
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
      <tr>
        <td data-label="Name">Front End Website 2.0</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">active</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/4/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/4">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Flights Module</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/5/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/5">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Front End Website 1.0</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/6/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/6">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Au Pair Front End</td>
        <td data-label="Through">Christian Wack (Smaller Earth DE)</td>
        <td data-label="Status">pending_review</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/7/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/7">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Front End Updates 1.5</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/8/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/8">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Branding</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/9/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/9">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Front End Website </td>
        <td data-label="Through">Kier Bates (USA Summer Camp)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/10/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/10">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">CRM Application</td>
        <td data-label="Through">Carlo Missirian (Human Utopia)</td>
        <td data-label="Status">proposal_sent</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/11/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/11">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Front End Website</td>
        <td data-label="Through">Carly Townsend (Roy Castle Lung Cancer Foundation)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/12/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/12">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Front End Website Refresh</td>
        <td data-label="Through">John Knight (Total Swimming)</td>
        <td data-label="Status">closed</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/13/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/13">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Luggage Tag</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">active</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/14/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/14">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Ticket Holder</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">active</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/15/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/15">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Camp Side - Hanover</td>
        <td data-label="Through">Bastian Weinberger (Smaller Earth Group)</td>
        <td data-label="Status">lead</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/16/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/16">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">External Linking and Menu Updates</td>
        <td data-label="Through">Christian Wack (Work and Traveller)</td>
        <td data-label="Status">pending_review</td>
        <td data-label="Amount">£0.00</td>
        <td data-label="View"><a href="show-project.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/projects/17/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/projects/17">Destroy</a></td>
      </tr>
  </tbody>
</table>

<br>

<a class="btn btn-primary pull-right" href="http://crm.cleversteam.com/projects/new">New Project</a>

</div>

<?php include('inc/footer.php') ?>