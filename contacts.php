<?php 
$page = "Contacts";
include('inc/header.php');
 ?>

<div class="container">
	<p id="notice"></p>

<h1>Listing Contacts</h1>

<table class="table table-striped">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Number</th>
      <th>Active project?</th>
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
      <tr>
        <td data-label="Name">Mike Peters</td>
        <td data-label="Email">m.peters@smallerearth.com</td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/1/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/1">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Bastian Weinberger</td>
        <td data-label="Email">b.weinberger@smallerearth.com</td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/2/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/2">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Christian Wack</td>
        <td data-label="Email">c.wack@smallerearth.com</td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-success glyphicon glyphicon-ok" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/3/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/3">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Carly Townsend</td>
        <td data-label="Email">carly.townsend@roycastle.org</td>
        <td data-label="Number">0333 323 7200 ext: 9191</td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/4/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/4">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Kier Bates</td>
        <td data-label="Email">kier.bates@usasummercamp.co.uk </td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/5/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/5">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">Carlo Missirian</td>
        <td data-label="Email">carlo@humanutopia.com </td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/6/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/6">Destroy</a></td>
      </tr>
      <tr>
        <td data-label="Name">John Knight</td>
        <td data-label="Email">john@thegoodmove.co.uk</td>
        <td data-label="Number"></td>
        <td data-label="Active"><span class="text-danger glyphicon glyphicon-remove" title=""></span></td>
        <td data-label="View"><a href="show-contact.php">Show</a></td>
        <td data-label="Update"><a href="http://crm.cleversteam.com/contacts/7/edit">Edit</a></td>
        <td data-label="Delete"><a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="http://crm.cleversteam.com/contacts/7">Destroy</a></td>
      </tr>
  </tbody>
</table>

<br>

<a class="btn btn-primary pull-right" href="http://crm.cleversteam.com/contacts/new">New Contact</a>

</div>

<?php include('inc/footer.php') ?>